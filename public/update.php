<?php

print 'Import file';

if (strstr($_SERVER['HTTP_HOST'],'.dev') !== false) {
    $mysqlDatabaseName ='valid';
    $mysqlUserName ='root';
    $mysqlPassword ='root';
    $mysqlHostName ='localhost';
    $mysqlImportFilename ='/var/www/backup/database.sql';
    //DONT EDIT BELOW THIS LINE
    //Export the database and output the status to the page
    $command='mysql -h' .$mysqlHostName .' -u' .$mysqlUserName .' -p' .$mysqlPassword .' ' .$mysqlDatabaseName .' < ' .$mysqlImportFilename;
    @exec($command,$output=array(),$worked);
    switch($worked){
        case 0:
            echo 'Import file <b>' .$mysqlImportFilename .'</b> successfully imported to database <b>' .$mysqlDatabaseName .'</b>';
            break;
        case 1:
            echo 'There was an error during import. Please make sure the import file is saved in the same folder as this script and check your values:<br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' .$mysqlDatabaseName .'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$mysqlUserName .'</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' .$mysqlHostName .'</b></td></tr><tr><td>MySQL Import Filename:</td><td><b>' .$mysqlImportFilename .'</b></td></tr></table>';
            break;
    }

    $command='rm -r /var/www/public/typo3temp/*';
    @exec($command,$output=array(),$worked);
}
#!/bin/sh

sudo cp /var/www/scripts/php.ini /etc/php5/apache2/php.ini
sudo apache2ctl graceful
echo "DB PHP.ini updated"

mysql -uroot -proot -e 'CREATE DATABASE valid;'
echo 'DB CREATED: valid';

#rm /var/www/backup/database.sql
#mysqldump --user=root --ignore-table=valid.sys_history --ignore-table=valid.sys_log --password=root --host=localhost valid > /var/www/backup/database.sql
#echo "DB BACKUP CREATED"


#INSTALL PHP7
#sudo apt-get update
#sudo add-apt-repository ppa:ondrej/php
#sudo apt-get install php7.0
#sudo apt-get update
#sudo apt-get install php7.0-mysql libapache2-mod-php7.0
##sudo a2dismod php5
#sudo a2enmod php7.0
#sudo apachectl restart
#echo "SWITCHED TO PHP7"